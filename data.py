def Articles():
    articles = [
        {
            'id': 1, 
            'title': 'Article one',
            'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint saepe dolores aut itaque, qui natus laborum, ducimus, temporibus quas beatae mollitia nihil cupiditate. Hic debitis harum magnam reiciendis laboriosam! Aliquam!',
            'author': 'JP',
            'create_date': '04-25-2017'
        },
         {
            'id': 2, 
            'title': 'Article Two',
            'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint saepe dolores aut itaque, qui natus laborum, ducimus, temporibus quas beatae mollitia nihil cupiditate. Hic debitis harum magnam reiciendis laboriosam! Aliquam!',
            'author': 'JP JP',
            'create_date': '04-25-2017'
        },
         {
            'id': 3, 
            'title': 'Article Three',
            'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint saepe dolores aut itaque, qui natus laborum, ducimus, temporibus quas beatae mollitia nihil cupiditate. Hic debitis harum magnam reiciendis laboriosam! Aliquam!',
            'author': 'JP JP',
            'create_date': '04-25-2017'
        },
    ]
    return articles